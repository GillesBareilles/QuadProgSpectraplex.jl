module QuadProgSpectraplex

using Printf
using DocStringExtensions
using LinearAlgebra
using FastLapackInterface
using Random

using ConjugateGradient

using TimerOutputs

const QPSp = QuadProgSpectraplex
export QPSp

@enum IterationStatus begin
    iteration_completed
    iteration_failed
    problem_solved
end

const warnimprecisesols = false

abstract type PointRepr end
struct AmbRepr <: PointRepr end
struct QuotRepr <: PointRepr end

include("types.jl")
include("utils.jl")
include("armijo.jl")

include("oracles.jl")
include("manifold_spectraplexface.jl")

include("up_projgradient.jl")
include("up_manifold.jl")

include("qpspectraplex.jl")

include("testinsts.jl")

export QPSpectraplex, QPSpectraplexState
export qpspectraplex, qpspectraplex!

export SpectraplexShadow


end
