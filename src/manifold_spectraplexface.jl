

function manifold_dimension(M::SymmPosSemidefFixedRankUnitTrace)
    # NOTE: not sure about this...
    return M.r*M.k - 1
end


@doc raw"""
    $(SIGNATURES)

Orthogonally project `d` on the tangent space of the current manifold at point `p`
and stores the result in `res`.

````math
\operatorname{proj}_{Y}(Z) = Z - Y\Omega - \frac{\langle Y, Z \rangle}{\langle Y, Y\rangle}
````
where $\Omega$ is a skew-symmetric matrix solving the following Lyapunov
equation:
````math
\Omega Y^\top Y + Y^\top Y \Omega = Y^\top Z - Z^\top Y.
````
"""
function project!(M::SymmPosSemidefFixedRankUnitTrace, res::Matrix{Tf}, Y::Matrix{Tf}, Z, projws::ProjectWs{Tf}, lyapws::LyapWs{Tf}) where {Tf}
    checksize(M, Y)
    checksize(M, Z)
    # tmp1 = zeros(Tf, M.k, M.k)
    # tmp2 = zeros(Tf, M.k, M.k)
    # tmp3 = zeros(Tf, M.r, M.k)
    # Ω = zeros(Tf, M.k, M.k)

    k, r = M.k, M.r
    tmp1 = @view projws.tmp1[1:k, 1:k]
    tmp2 = @view projws.tmp2[1:k, 1:k]
    tmp3 = @view projws.tmp3[1:r, 1:k]
    Ω =    @view projws.tmp4[1:k, 1:k]
    # tmp1 = M.tempkk1
    # tmp2 = M.tempkk2
    # tmp3 = M.temprk1
    # Ω = M.tempkk3

    # @show size(Y)
    # @show size(Z)
    # @show size(res)

    # @timeit_debug "lyap pr setup" begin
    mul!(tmp1, Z', Y)
    tmp2 .= tmp1 .- tmp1'
    mul!(tmp1, Y', Y)
    # end
    # @timeit_debug "lyap1"

    # lyapstorage = LyapWs(M.r)

    lyap!(Ω, tmp1, tmp2, M, lyapws)
    # lyap(tmp1, tmp2)
    # @assert norm(Ω - lyap(tmp1, tmp2)) < 1e-14

    # if M.r != M.k
    #     @assert false
    # end

    # @assert norm(tmp1 - Y' * Y) < 5e-14
    # @assert norm(tmp2 - (Z' * Y - Y' * Z)) < 5e-14
    # @assert norm(Ω - lyap(Y' * Y, Z' * Y - Y' * Z)) < 5e-14

    # @timeit_debug "res pr eff" begin
    # res .= Z .- Y * Ω .- (tr(Z' * Y) / tr(Y' * Y)) .* Y
    res .= Z
    mul!(tmp3, Y, Ω)
    res .= Z .- tmp3
    res .-= (dot(Z, Y) ./ dot(Y, Y)) .* Y
    # @assert isapprox(res, Z .- Y * Ω .- (tr(Z' * Y) / tr(Y' * Y)) .* Y) || norm(res - (Z .- Y * Ω .- (tr(Z' * Y) / tr(Y' * Y)) .* Y)) < 1e-14
    # end
    return nothing
end

# function project!(::SymmPosSemidefFixedRankUnitTrace{r, k}, res, Y, Z) where {r, k}
#     if size(Y) != (r, k) || size(Z) != (r, k)
#         throw(error("Input point and vector sizes should be ($k, $r) but are $(size(Y)), $(size(Z))."))
#     end
#     Ω = zeros(k, k)
#     try
#         Ω = lyap(Y' * Y, Z' * Y - Y' * Z)
#     catch e
#         @warn "Failed to solve lyapunov system" e r k
#         display(Y)
#     end

#     res .= Z .- Y * Ω .- (tr(Z' * Y) / tr(Y' * Y)) .* Y
#     return res
# end




@doc raw"""
    $(SIGNATURES)

Compute the projection retraction of vector `d` at point `p`
"""
function retract!(M::SymmPosSemidefFixedRankUnitTrace, res, P, D)
    checksize(M, P)
    checksize(M, D)

    res .= P .+ D
    res ./= sqrt(dot(res, res))
    return res
end

function ehess2rhess!(M::SymmPosSemidefFixedRankUnitTrace, res, x, ∇fₓ, ∇²fₓξ, ξ, solverws::SolverWs)
    # @timeit_debug "checksize" begin
    checksize(M, x)::Nothing
    checksize(M, ξ)::Nothing
    # end

    # @timeit_debug "allocs" begin
    # tmp1 = similar(res, M.k, M.k)
    # tmp2 = similar(res, M.k, M.k)
    # tmp3 = similar(res, M.r, M.k)
    # tmp4 = similar(res, M.r, M.k)

    # rhs = similar(res, M.k, M.k)
    # Ωᴰ = similar(res, M.k, M.k)

    k, r = M.k, M.r
    tmp1 = @view solverws.ehess2rhessws.tmp1[1:k, 1:k]
    tmp2 = @view solverws.ehess2rhessws.tmp2[1:k, 1:k]
    tmp3 = @view solverws.ehess2rhessws.tmp3[1:r, 1:k]
    tmp4 = @view solverws.ehess2rhessws.tmp4[1:r, 1:k]

    rhs = @view solverws.ehess2rhessws.tmp5[1:k, 1:k]
    Ωᴰ = @view solverws.ehess2rhessws.tmp6[1:k, 1:k]
    Ω = zeros(Float64, k, k)

    # end


    ## Block 1
    # @timeit_debug "lyap eff" begin
        # Ω = lyap(x'*x, x'*∇fₓ-∇fₓ'*x)
        mul!(tmp1, x', x)
        mul!(tmp2, x', ∇fₓ)
        tmp2 .-= tmp2'

        lyap!(Ω, tmp1, tmp2, M, solverws.lyapws)
        # Ω = lyap(tmp1, tmp2)
    # end
    # @assert norm(Ω - lyap(x'*x, x'*∇fₓ-∇fₓ'*x)) < 1e-14

    ## Block 2
    # @timeit_debug "rhs eff" begin
        # rhs = ξ'*∇fₓ-∇fₓ'*ξ + x'*∇²fₓξ-∇²fₓξ'*x - Ω*(x'*ξ+ξ'*x)-(x'*ξ+ξ'*x)*Ω
        mul!(tmp1, ξ', ∇fₓ)
        rhs .= tmp1 .- tmp1'

        mul!(tmp1, x', ∇²fₓξ)
        rhs .+= tmp1 .- tmp1'

        mul!(tmp2, x', ξ)
        tmp1 .= tmp2 .+ tmp2'
        mul!(tmp2, Ω, tmp1)
        rhs .-= tmp2 .+ tmp2'
    # end
    # @assert norm(rhs - (ξ'*∇fₓ-∇fₓ'*ξ + x'*∇²fₓξ-∇²fₓξ'*x - Ω*(x'*ξ+ξ'*x)-(x'*ξ+ξ'*x)*Ω)) < 1e-12

    ## Block
    # @timeit_debug "lyap2 eff" begin
        mul!(tmp1, x', x)

        # Ωᴰ .= lyap(tmp1, rhs)
        lyap!(Ωᴰ, tmp1, rhs, M, solverws.lyapws)
    # end
    # @assert norm(Ωᴰ - lyap(x'*x, rhs)) < 1e-14

    ## Block
    # @timeit_debug "res eff" begin
        # res .= ∇²fₓξ -ξ*Ω-x*Ωᴰ-tr(ξ'*∇fₓ)*x-tr(x'*∇²fₓξ)*x-tr(x'*∇fₓ)*ξ
        tmp3 .= ∇²fₓξ

        mul!(tmp4, ξ, Ω)
        tmp3 .-= tmp4
        mul!(tmp4, x, Ωᴰ)
        tmp3 .-= tmp4

        tmp3 .-= dot(ξ, ∇fₓ) .* x
        tmp3 .-= dot(x, ∇²fₓξ) .* x
        tmp3 .-= dot(x, ∇fₓ) .* ξ
    # end
    # @assert isapprox(tmp3, ∇²fₓξ -ξ*Ω-x*Ωᴰ-tr(ξ'*∇fₓ)*x-tr(x'*∇²fₓξ)*x-tr(x'*∇fₓ)*ξ)

    # @timeit_debug "project"
    project!(M, res, x, tmp3, solverws.projectws, solverws.lyapws)::Nothing
    return nothing
end


################################################################################
### Utilities
################################################################################

function mat_to_lowrankmat(::SymmPosSemidefFixedRankUnitTrace, Z)
end

function amb_to_manifold_repr(M::SymmPosSemidefFixedRankUnitTrace, x::Tm) where {Tf, Tm <: AbstractMatrix{Tf}}
    λs, E = eigen(Symmetric(x))
    λs .= max.(λs, 0)

    Y = zeros(Tf, M.r, M.k)
    for i in 1:M.k
        Y[:, i] .= sqrt(λs[end-i+1]) * E[:, end-i+1]
    end
    return Y
end
function manifold_to_amb_repr(::SymmPosSemidefFixedRankUnitTrace, x)
    return x * x'
end
manifold_repr(::SymmPosSemidefFixedRankUnitTrace) = QuotRepr()


function checksize(M::SymmPosSemidefFixedRankUnitTrace, P)
    if size(P) != (M.r, M.k)
        @error "Input point and vector sizes are incompatible with quotient representation."
    end
    return nothing
end
