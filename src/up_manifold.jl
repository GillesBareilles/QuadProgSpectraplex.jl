function newton_manifold!(state::QPSpectraplexState{Tf}, x::Matrix{Tf}, set::SpectraplexShadow, M; show_trace = false) where {Tf}
    x_man = amb_to_manifold_repr(M, x)
    manrepr = manifold_repr(M)

    ∇fₓ = similar(x_man)
    ∇²fxd = similar(x_man)
    gradFₓ = similar(x_man)
    dₜ = similar(x_man)
    res = similar(x_man)

    # Compute Riemannian gradient and hessian of f, on quotient representation
    # @timeit_debug "nablaf" begin
    ∇f!(∇fₓ, set, x_man, manrepr)
    project!(M, gradFₓ, x_man, ∇fₓ, state.ws.projectws, state.ws.lyapws)
    gradnorm = norm(gradFₓ)
    # end

    function HessFₓη!(res, d)
        # @timeit_debug "project" project!(M, dₜ, x_man, d, state.ws.projectws, state.ws.lyapws)
        # @timeit_debug "nabla2" ∇²f!(∇²fxd, set, x_man, dₜ, manrepr)
        # @timeit_debug "ehessrhess" ehess2rhess!(M, res, x_man, ∇fₓ, ∇²fxd, dₜ, state.ws)

        project!(M, dₜ, x_man, d, state.ws.projectws, state.ws.lyapws)
        ∇²f!(∇²fxd, set, x_man, dₜ, manrepr)
        ehess2rhess!(M, res, x_man, ∇fₓ, ∇²fxd, dₜ, state.ws)
        return res
    end

    # @timeit_debug "Riemann grad" begin
    # end

    # Solve Newton's equation
    tol = max(1e-3 * min(0.5, (gradnorm)^0.5) * gradnorm, eps(Tf))
    # @timeit_debug "solve_tCG" begin
    dᴺ::Matrix{Tf}, CGstats = solve_tCG(HessFₓη!, -gradFₓ; ν=1e-15, ϵ_residual = tol, maxiter=2*manifold_dimension(M), printlev=0)
    # end

    # Linesearch step
    xcand = copy(x_man)
    α, fx = performlinesearch(x_man, xcand, M, dᴺ, set, gradFₓ, manrepr)

    x_old = copy(x)
    retract!(M, x_man, x_man, α .* dᴺ)
    x .= manifold_to_amb_repr(M, x_man)

    show_trace && display_man_logs(fx, x_old, x, gradnorm, M, tol, dᴺ, α, CGstats)

    return nothing
end

function display_man_logs(fx, x_old, x, gradnorm, M, tol, dᴺ, α, CGstats)
    @printf "%.16e   %.2e  %.2e  | " fx norm(x_old - x) gradnorm
    @printf " %i  %.2e  %.2e  %.2e  %2i     %s\n" M.k tol norm(dᴺ) α CGstats.iter CGstats.d_type
    return nothing
end


function retract_f!(set, M, x_man, xcand, t, dᴺ)
    retract!(M, xcand, x_man, t .* dᴺ)
    return f(set, xcand, QuotRepr())
end

function performlinesearch(x_man::Matrix{Tf}, xcand::Matrix{Tf}, M, dᴺ, set, gradFₓ, manrepr) where Tf
    xcand .= x_man

    Fₓ = f(set, x_man, manrepr)
    initslope = dot(gradFₓ, dᴺ)

    (initslope > 0) && @warn "ascent direction in linesearch" initslope
    if initslope / (norm(gradFₓ) * norm(dᴺ)) > -1e-14
        @warn "exiting linesearch: almost not descent dir"
        return 1.0, Fₓ
    end

    update_xf! = (xcand, t) -> retract_f!(set, M, x_man, xcand, t, dᴺ)
    t, F_cand = linesearch!(xcand, update_xf!, x_man, Fₓ, initslope)

    return t, F_cand
end

