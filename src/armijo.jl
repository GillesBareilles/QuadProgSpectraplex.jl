Base.@kwdef struct Armijo{Tf}
    α::Tf = 1e-4
    maxit::Int64 = 50
end

Base.@kwdef mutable struct ArmijoState{Tf}
    cur_funval::Tf = Inf
    prev_funval::Tf = Inf
    prev_initslope::Tf = Inf
end

"""
    $TYPEDSIGNATURES

Performs a backtracking linesearch to meet the Armijo condition, following
algorithm A.6.3.1, p. 326 of:
> Dennis, Schnabel (1996) Numerical Methods for Unconstrained Optimization and Nonlinear Equations, Society for Industrial and Applied Mathematics.

Arguments:
- `update_xf!`: function of `(x_cand, λ)` that computes in-place, in `res`, a
  step of length `λ` and returns the function value there.
"""
function linesearch!(x_cand::Tv, update_xf!, x::Tv, Fₓ::Tf, initslope::Tf; hist = Dict()) where {Tf, Tv <: AbstractArray{Tf}}
    linesearchstate = ArmijoState{Tf}()
    ls = Armijo{Tf}()

    ## Check quality of step

    maxtaken = false
    retcode = :IncompleteExecution

    minlambda = 1e-15

    F_cand = Inf
    F_candprev = Inf
    x_cand .= x

    linesearchstate.cur_funval = Fₓ
    λ = Tf(1.0)
    λprev = Tf(1.0)

    it_ls = 0
    while retcode == :IncompleteExecution
        F_cand = update_xf!(x_cand, λ)

        if Fₓ > F_cand > Fₓ - 3*eps(F_cand)
            @debug "Armijo linesearch: reached conditionning of funtion here, #it ls: $it_ls"
            break
        end

        if F_cand ≤ Fₓ + ls.α*λ*initslope
            retcode = :Satisfactory
            if λ==1 #&& (newtlen > 0.99*maxstep)
                maxtaken = true
            end
            break
        elseif λ < minlambda
            retcode = :Failure
            @debug "Armijo linesearch: too small step" λ minlambda
            x_cand .= x
            break
        else
            if λ == 1.0
                λtemp = -initslope / (2*(F_cand - Fₓ - initslope))
            else
                a, b = 1/(λ-λprev) * [
                    1/λ^2 -1/(λprev^2)
                    -λprev/λ^2  λ/(λprev^2)
                ] * [
                    F_cand - Fₓ - λ*initslope
                    F_candprev - Fₓ - λprev*initslope
                ]
                disc = b^2 - 3*a*initslope
                if a==0
                    λtemp = -initslope / (2b)
                else
                    λtemp = (-b+sqrt(disc)) / (3a)
                end

                λtemp = min(λtemp, 0.5λ)
            end

            λprev = λ
            F_candprev = F_cand

            λ = max(λtemp, 0.1λ)
        end

        it_ls += 1
        (it_ls > ls.maxit) && (break)
    end

    hist[:niter] = it_ls
    hist[:retcode] = retcode
    hist[:maxtaken] = maxtaken

    ## Update linesearch state
    linesearchstate.prev_funval = Fₓ
    linesearchstate.prev_initslope = initslope

    return λ, F_cand
end
