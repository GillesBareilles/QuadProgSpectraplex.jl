function initial_state(::QPSpectraplex{Tf}, set::SpectraplexShadow) where Tf
    r = set.r
    return QPSpectraplexState(
        x = zeros(Tf, r, r) + I/r,
        M = SymmPosSemidefFixedRankUnitTrace{Tf}(r, r),
        L = Tf(1),
        x_old = zeros(Tf, r, r) + I/r,
        ∇fx = zeros(Tf, r, r) + I/r,
        u = zeros(Tf, r, r) + I/r,
        ws = SolverWs(r; Tf)
    )
end

#
### Printing
#
print_header(::QPSpectraplex) = println("**** QPSpectraplex algorithm")
function display_logs_header_bt(::QPSpectraplex)
    print("f(x)                     step      |grad f|  |  it_bt\n")
    return nothing
end
function display_logs_header_man(::QPSpectraplex)
    print( "f(x)                     step      |grad f|  |  M  tol       |dᴺ|      α         CGiter status\n")
    return nothing
end

function display_logs(state, ::QPSpectraplex; time_count, set)
    @printf "%4i  %.1e   %-.16e   %.2e  %s\n" state.it time_count f(set, state.x, AmbRepr()) state.L state.M
    return nothing
end

raw"""
    $TYPEDSIGNATURES

Solve the problem $min_{x \in \Delta} 0.5 \|Px\|^2 + \langle a, x\rangle$ where $x$
is constrained to the spectraplex set $\Delta$ (non-negative coordinates that sum
to one).

Reference:
- Kiwiel (1986) A Method for Solving Certain Quadratic Programming Problems
  Arising in Nonsmooth Optimization, IMA Journal of Numerical Analysis.
"""
function qpspectraplex(set::SpectraplexShadow; show_trace = false, check_optimality=false, iterations_limit = 100)
    Tf = eltype(first(set.As))
    o = QPSpectraplex(; Tf)
    state = initial_state(o, set)
    qpspectraplex!(state, o, set; show_trace, iterations_limit)

    return state.x, state.M
end

function qpspectraplex!(
    state::QPSpectraplexState{Tf},
    optimizer::QPSpectraplex{Tf},
    set::SpectraplexShadow;
    show_trace = false,
    iterations_limit = 100,
) where Tf
    time_count = 0.0
    iteration = 0
    converged = false
    stopped = false

    if show_trace
        print_header(optimizer)
        display_logs_header_bt(optimizer)
        display_logs_header_man(optimizer)
    end

    while !converged && !stopped && iteration < iterations_limit
        iteration += 1

        _time = time()
        iterationstatus = update_iterate!(state, optimizer, set; show_trace)
        time_count += time() - _time

        stopped = (iterationstatus == iteration_failed)
        converged = (iterationstatus == problem_solved)
    end
    !converged && @error "not converged"
    return nothing
end

#
### QPSpectraplex method
#
raw"""
    $TYPEDSIGNATURES
"""
function update_iterate!(state::QPSpectraplexState{Tf}, o::QPSpectraplex{Tf}, set::SpectraplexShadow; show_trace = false) where {Tf}
    x::Matrix{Tf} = state.x
    M = state.M
# state.L, state.x_old, state.∇fx, state.u

    repr = AmbRepr()

    state.x_old .= x
    @timeit_debug "newtonman" newton_manifold!(state, x, set, M; show_trace)

    ## backtracking procedure for proxgrad step
    state.x_old .= x
    @timeit_debug "backtracking" state.M, state.L, it_btbeck = backtracked_proxgrad!(x, set, state.L, state.x_old, state.∇fx, state.u, repr; showls = false, show_trace)
    step_pg = norm(state.x_old - x)

    state.x_old .= x
    state.it += 1

    if step_pg < o.ϵ
        return problem_solved
    elseif state.it > o.itmax
        return iteration_failed
    end

    state.x_old .= x

    return iteration_completed
end

get_minimizer_candidate(state::QPSpectraplexState) = state.x
get_activeface_candidate(state::QPSpectraplexState) = state.M


