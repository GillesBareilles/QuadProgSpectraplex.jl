## Storage
struct LyapWs{Tf}
    tmp1::Matrix{Tf}
    tmp2::Matrix{Tf}
    tmp3::Matrix{Tf}
    tmp4::Matrix{Tf}
    ws::SchurWs{Tf}
    LyapWs(r; Tf = Float64) = new{Tf}(
        zeros(Tf, r, r),
        zeros(Tf, r, r),
        zeros(Tf, r, r),
        zeros(Tf, r, r),
        SchurWs(zeros(Tf, r, r)),
    )
end

struct ProjectWs{Tf}
    tmp1::Matrix{Tf}
    tmp2::Matrix{Tf}
    tmp3::Matrix{Tf}
    tmp4::Matrix{Tf}
    ProjectWs(r; Tf = Float64) = new{Tf}(
        zeros(Tf, r, r),
        zeros(Tf, r, r),
        zeros(Tf, r, r),
        zeros(Tf, r, r),
    )
end

struct Ehess2rhessWs{Tf}
    tmp1::Matrix{Tf}
    tmp2::Matrix{Tf}
    tmp3::Matrix{Tf}
    tmp4::Matrix{Tf}
    tmp5::Matrix{Tf}
    tmp6::Matrix{Tf}
    Ehess2rhessWs(r; Tf = Float64) = new{Tf}(
        zeros(Tf, r, r),
        zeros(Tf, r, r),
        zeros(Tf, r, r),
        zeros(Tf, r, r),
        zeros(Tf, r, r),
        zeros(Tf, r, r),
    )
end

struct SolverWs{Tf}
    lyapws::LyapWs{Tf}
    projectws::ProjectWs{Tf}
    ehess2rhessws::Ehess2rhessWs{Tf}
    SolverWs(r; Tf = Float64) = new{Tf}(
        LyapWs(r; Tf),
        ProjectWs(r; Tf),
        Ehess2rhessWs(r; Tf),
    )
end

"""
    $(SIGNATURES)

The manifold of symmetric positive semidefinite matrices of size `r`, rank `k`
and unit trace.

# Representation:
- Manifold points are represented as vectors of size `rk`
- Tangent vectors are represented as vectors of size `rk`.

# Reference:
- Journ\'ee, M., Bach, F., Absil, P., & Sepulchre, R. (2010). Low-rank
  optimization for semidefinite convex problems. SIAM Journal on Optimization,
  20(5), 2327–2351. http://dx.doi.org/10.1137/080731359
"""
struct SymmPosSemidefFixedRankUnitTrace{Tf}
    r::Int64
    k::Int64
    tempkk1::Matrix{Tf}
    tempkk2::Matrix{Tf}
    tempkk3::Matrix{Tf}
    tempkk4::Matrix{Tf}
    temprk1::Matrix{Tf}
    temprk2::Matrix{Tf}
    function SymmPosSemidefFixedRankUnitTrace{Tf}(r, k) where Tf
        return new(r, k,
                   zeros(Tf, k, k),
                   zeros(Tf, k, k),
                   zeros(Tf, k, k),
                   zeros(Tf, k, k),
                   zeros(Tf, r, k),
                   zeros(Tf, r, k),
                   )
    end
end

## Solver types
struct QPSpectraplex{Tf}
    itmax::Int64
    ϵ::Tf
end

QPSpectraplex(;Tf = Float64) = QPSpectraplex{Tf}(
    1e2,
    1e2 * eps(Tf),
)

Base.@kwdef mutable struct QPSpectraplexState{Tf}
    x::Matrix{Tf}
    M::SymmPosSemidefFixedRankUnitTrace{Tf}
    L::Tf
    x_old::Matrix{Tf}
    ∇fx::Matrix{Tf}
    u::Matrix{Tf}
    ws::SolverWs{Tf}
    it::Int64 = -1
end

