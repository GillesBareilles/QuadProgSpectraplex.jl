"""
    $(TYPEDSIGNATURES)

Projects α on the unit simplex.

### Reference:
- Fast Projection onto the Simplex and the ℓ1 Ball, L. Condat, alg. 1
"""
function project_simplex!(res::Vector{Tf}, α::Vector{Tf}) where {Tf}
    N = length(α)

    # 1. Sorting
    u = sort(α, rev = true)

    # 2.
    k = 1
    sum_u_1k = u[1]
    while (k < N) && (sum_u_1k + u[k+1] - 1) / (k + 1) < u[k+1]
        k += 1
        sum_u_1k += u[k]
    end

    # 3.
    τ = (sum_u_1k - 1) / k

    res .= @. max(α - τ, Tf(0))
    return nothing
end



function lyap!(res, Ain::StridedMatrix{Tf}, Cin::StridedMatrix{Tf}, M, lst::LyapWs{Tf}) where {Tf<:LinearAlgebra.BlasFloat}
    k = M.k

    tmp1 = @view lst.tmp1[1:k, 1:k]
    D = @view lst.tmp2[1:k, 1:k]
    tmp2 = @view lst.tmp3[1:k, 1:k]
    A = @view lst.tmp4[1:k, 1:k]

    A .= Ain

    # R, Q = schur(A)
    ## Leave A unchanged
    LAPACK.gees!(lst.ws, 'V', A)

    R = A         # upper triangular matrix
    Q = lst.ws.vs # orthogonal matrix

    mul!(tmp1, Cin, Q)
    mul!(D, Q', tmp1)
    D .*= -1
    # @assert norm(D + (adjoint(Q) * (C*Q))) < 1e-14

    Y, scale = LAPACK.trsyl!('N', Tf <: Complex ? 'C' : 'T', R, R, D)

    mul!(tmp1, Y, Q')
    mul!(tmp2, Q, tmp1)
    mul!(res, tmp2, inv(scale))
    # @assert norm(res - rmul!(Q*(Y * adjoint(Q)), inv(scale))) < 1e-14
    return nothing
end


# """
#     $(SIGNATURES)

# Computes the solution `X` to the continuous Lyapunov equation `AX + XA' + C = 0`
# for arbitraty type, based on the `IterativeSolvers.idrs` routine.
# """
# function lyap!(res::Tm, A::Tm, C::Tm) where {Tf, Tm <: AbstractMatrix{Tf}}
#     r = size(A, 1)
#     if size(A) != (r, r) || size(C) != (r, r)
#         throw(error())
#     end

#     function lyapop(res::Vector, x::Vector)
#         X = reshape(x, (r, r))
#         res .= vec(A*X)
#         res .+= vec(X*A')
#         return
#     end

#     O = LinearMap{Tf}(lyapop, r^2, r^2, issymmetric=true)
#     return reshape(idrs(O, -vec(C)), (r, r))
# end
