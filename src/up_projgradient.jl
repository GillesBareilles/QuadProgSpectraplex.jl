"""
    $(SIGNATURES)

Perform a backtacked proximal gradient step, following procedure `B1`, section
10.3 from Beck's book.

## Reference:
- Beck, A. (2017). First-Order Methods in Optimization. Philadelphia, PA:
  Society for Industrial and Applied Mathematics.
"""
function backtracked_proxgrad!(TxL, set, L, xₖ, ∇fx, u, repr; maxiter = 100, showls = false, show_trace = false)
    ∇f!(∇fx, set, xₖ, repr)

    Tf = eltype(TxL)
    s = 1e-10
    η = 2
    γᵇ = 0.5

    u .= xₖ .- (1 / L) .* ∇fx
    structure = prox_γg!(TxL, set, u, repr)

    Fxₖ = f(set, xₖ, repr)
    FTxL = f(set, TxL, repr)

    it_btbeck = 0

    showls && @printf "*it   f(x)            f(Tᴸ(x))           f(x)-f(Tᴸ(x)) γL|Tᴸ(x)-x|²      L          |Tᴸ(x)-x|²  M\n"
    showls && @printf "*%3i  %.8e  %.8e        % .3e %.3e         %.3e  %.3e   %s\n" it_btbeck Fxₖ FTxL Fxₖ-FTxL γᵇ * L * norm(xₖ-TxL)^2 L norm(xₖ-TxL)^2 structure
    while Fxₖ - FTxL < γᵇ * L * norm(xₖ - TxL)^2 - 10 * eps(Tf)
        # the above eltype is meant to stop the linesearch when the values are equal up to machine precision.
        # This happens generally when they are equal to zero.

        L *= η

        u .= xₖ .- (1 / L) .* ∇fx
        structure = prox_γg!(TxL, set, u, repr)
        FTxL = f(set, TxL, repr)

        it_btbeck += 1
        showls && @printf "*%3i  %.8e  %.8e        % .3e %.3e         %.3e  %.3e   %s\n" it_btbeck Fxₖ FTxL Fxₖ-FTxL γᵇ * L * norm(xₖ-TxL)^2 L norm(xₖ-TxL)^2 structure

        if it_btbeck > maxiter
            @warn "Many backtracking steps here, passing"
            # throw(error("Many backtracking steps here"))
            break
        end
    end

    show_trace && display_bt_logs(FTxL, xₖ, TxL, it_btbeck)

    return structure, L, it_btbeck
end

function display_bt_logs(FTxL, xₖ, TxL, it_btbeck)
    @printf "%.16e   %.2e  %.2e  | " FTxL norm(xₖ - TxL) 0
    @printf " %i\n" it_btbeck
    return nothing
end
