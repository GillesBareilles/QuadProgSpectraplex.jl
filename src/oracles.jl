raw"""
    SpectraplexShadow{Tm}

Models the problem of projecting zero on a shadow of the spectraplex as
\min 0.5 * || A Z ||^2, s.t. Z \in \mathcal S_r, Z \succeq 0, tr(Z) = 1,
using the Frobenius norm.
"""
struct SpectraplexShadow{Tm}
    As::Vector{Tm}
    r::Int64
    # function SpectraplexShadow(Tf, As::Vector{Tm}, r) where Tm
    #     if typeof(first(As[1])) != Tf
    #         throw(error("Floating precision type inconsistent with shadow vectors"))
    #     end
    #     return new{Tf, Tm}(As, r)
    # end
end

# function SpectraplexShadow(As::Vector{LinearAlgebra.Symmetric{Tf, Matrix{Tf}}}, r::Int64) where {Tf}
#     return SpectraplexShadow(As, r)
# end




################################################################################
## Ambient representation
################################################################################
function f(set::SpectraplexShadow, Z, ::AmbRepr)
    return 0.5 * sum(dot(A, Z)^2 for A in set.As)
end

function ∇f!(res::Matrix{Tf}, sh::SpectraplexShadow, Z::Matrix{Tf}, ::AmbRepr) where {Tf}
    res .= Tf(0)
    @inbounds for i in 1:length(sh.As)
        Aᵢ = sh.As[i]
        ps = dot(Aᵢ, Z)
        res .+= ps .* Aᵢ
    end
    return nothing
end

"""
    $(SIGNATURES)

Indicator function of the spectraplex set.
"""
function g(::SpectraplexShadow, X::Matrix{Tf}, ::AmbRepr) where Tf
    tol = 50 * eps(Tf)

    Λ = eigvals(Symmetric(X*X'))

    norm(sum(Λ) - Tf(1)) > tol && return Tf(Inf)
    norm(min.(Λ, 0)) > tol && return Tf(Inf)

    return Tf(0)
end

function prox_γg!(res, set::SpectraplexShadow, Z::Matrix{Tf}, ::AmbRepr) where Tf
    Λ, E = eigen(Symmetric(Z))

    Λ_prox = zeros(Tf, size(Λ))
    project_simplex!(Λ_prox, Λ)
    rank = sum( Λ_prox .!= 0 )

    res .= E * Diagonal(Λ_prox) * E'
    return SymmPosSemidefFixedRankUnitTrace{Tf}(set.r, rank)
end


################################################################################
## Quotient representation
################################################################################
function f(sh::SpectraplexShadow, X, ::QuotRepr)
    return 0.5 * sum(dot(A, X * X')^2 for A in sh.As)
end

function barrier_∇f!(res, Aᵢ, X)
    ## TODO: intermediate alloc here
    res .+= 2 .* dot(Aᵢ, X*X') .* Aᵢ*X
    return
end
function ∇f!(res::Matrix{Tf}, sh::SpectraplexShadow, X::Matrix{Tf}, ::QuotRepr) where Tf
    res .= 0
    for i in 1:length(sh.As)
        Aᵢ = sh.As[i]
        barrier_∇f!(res, Aᵢ, X)
    end
    return nothing
end

function barrier_∇²f!(res, AD, AX, X, A, D)
    mul!(AX, A, X)
    mul!(AD, A, D)

    size(A), size(X), size(AD)

    res .+= 4 .* dot(AX, D) .* AX
    res .+= 2 .* dot(AX, X) .* AD
    return nothing
end
function ∇²f!(res, set::SpectraplexShadow, X, D, ::QuotRepr)
    res .= 0
    AD = similar(X)
    AX = similar(X)
    @inbounds for A in set.As
        barrier_∇²f!(res, AD, AX, X, A.data, D)
    end
    return nothing
end

function g(::SpectraplexShadow, X::Matrix{Tf}, ::QuotRepr) where Tf
    tol = 50 * eps(Tf)

    σ = svdvals(X)
    norm(sum(σ.^2) - Tf(1)) > tol && return Tf(Inf)

    return Tf(0)
end
function prox_γg!(res::Matrix{Tf}, sh::SpectraplexShadow, X::Matrix{Tf}, ::QuotRepr) where Tf
    res .= X / norm(X)
    return SymmPosSemidefFixedRankUnitTrace{Tf}(sh.r, 5)
end
