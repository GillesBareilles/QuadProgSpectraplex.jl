# QuadProgSpectraplex.jl

<!-- [![Stable](https://img.shields.io/badge/docs-stable-blue.svg)](https://GillesBareilles.github.io/QuadProgSpectraplex.jl/stable) -->
<!-- [![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://GillesBareilles.github.io/QuadProgSpectraplex.jl/dev) -->
<!-- [![Build Status](https://github.com/GillesBareilles/QuadProgSpectraplex.jl/actions/workflows/CI.yml/badge.svg?branch=main)](https://github.com/GillesBareilles/QuadProgSpectraplex.jl/actions/workflows/CI.yml?query=branch%3Amain) -->
<!-- [![Coverage](https://codecov.io/gh/GillesBareilles/QuadProgSpectraplex.jl/branch/main/graph/badge.svg)](https://codecov.io/gh/GillesBareilles/QuadProgSpectraplex.jl) -->
[![Code Style: Blue](https://img.shields.io/badge/code%20style-blue-4495d1.svg)](https://github.com/invenia/BlueStyle)

Minimize quadratic over the spectraplex, fast and accurate.

## Algorithm & usage

In construction.
