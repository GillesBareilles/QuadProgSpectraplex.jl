using JLD2
using BenchmarkTools
using TimerOutputs
using LinearAlgebra

function qpspecto()
    TimerOutputs.reset_timer!(TimerOutputs.DEFAULT_TIMER)
    sset = load("specdiffinstance.jld2")["set"]
    set = SpectraplexShadow(sset.As, sset.r)
    @timeit_debug "alg" qpspectraplex(set)
    TimerOutputs.complement!(TimerOutputs.DEFAULT_TIMER)
    show(TimerOutputs.DEFAULT_TIMER)
    println()
    @time qpspectraplex(set)
    return
end

function loadAsr(path)
    As::Vector{LinearAlgebra.Symmetric{Float64, Matrix{Float64}}} = load(path)["As"]
    r::Int64 = load(path)["r"]
    return As, r
end
function qpspecto2()
    TimerOutputs.reset_timer!(TimerOutputs.DEFAULT_TIMER)
    As, r = loadAsr("diffinst2.jld2")
    set = SpectraplexShadow(As, r)

    @timeit_debug "alg" qpspectraplex(set)
    TimerOutputs.complement!(TimerOutputs.DEFAULT_TIMER)
    show(TimerOutputs.DEFAULT_TIMER)
    println()
    qpspectraplex(set; show_trace = true)

    println()
    println()
    println()
    @time qpspectraplex(set; show_trace = false)
    return
end

function testehess()
    # TimerOutputs.enable_debug_timings(ConvexHullProjection)
    (M, res, x_man, ∇fₓ, ∇²fxd, dₜ) = (SymmPosSemidefFixedRankUnitTrace(2, 2), [-0.47819605966437395 -67.82478217655463; -67.82478217655463 0.47819605966437395], [-0.0 -0.7071067811865476; 0.7071067811865476 -0.0], [-0.12474802702652342 -24.887874686058716; 32.85370249486877 0.12474802702652342], [4.615198617327215 -163.0754167053693; -297.8146017560698 -4.615198617327208], [0.12474802702652342 -3.9829139044050272; -3.9829139044050272 -0.12474802702652342])
    @time ehess2rhess!(M, res, x_man, ∇fₓ, ∇²fxd, dₜ)

    TimerOutputs.reset_timer!(TimerOutputs.DEFAULT_TIMER)
    @timeit_debug "ehessrhess" ehess2rhess!(M, res, x_man, ∇fₓ, ∇²fxd, dₜ)
    TimerOutputs.complement!(TimerOutputs.DEFAULT_TIMER)
    show(TimerOutputs.DEFAULT_TIMER)
    println()

    res2 = [-0.47819605966437395 -67.82478217655463; -67.82478217655463 0.47819605966437395]
    if norm(res - res2) > 1e-14
        @show res
        @show res2
        @assert false
    end
    return
    return @benchmark ehess2rhess!($M, $res, $x_man, $∇fₓ, $∇²fxd, $dₜ)
end

# BenchmarkTools.Trial: 10000 samples with 3 evaluations.
#  Range (min … max):   9.333 μs …  2.186 ms  ┊ GC (min … max): 0.00% … 99.09%
#  Time  (median):     11.652 μs              ┊ GC (median):    0.00%
#  Time  (mean ± σ):   13.321 μs ± 36.166 μs  ┊ GC (mean ± σ):  4.67% ±  1.72%

#              █▇
#   ▃█▄▄▃▄▃▂▄▃▂██▇▃▂▁▁▁▁▂▂▂▂▂▁▂▄▄▂▅▆▄▄▄▄▂▂▂▂▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁ ▂
#   9.33 μs         Histogram: frequency by time        20.1 μs <

#  Memory estimate: 8.53 KiB, allocs estimate: 81.
# BenchmarkTools.Trial: 10000 samples with 1 evaluation.
#  Range (min … max):  10.465 μs … 49.459 μs  ┊ GC (min … max): 0.00% … 0.00%
#  Time  (median):     13.305 μs              ┊ GC (median):    0.00%
#  Time  (mean ± σ):   13.525 μs ±  1.812 μs  ┊ GC (mean ± σ):  0.00% ± 0.00%

#          ▅▅     ▂█▇▅▄▃▁
#   ▁▃▃▃▃▂▄███▇▆▅▄███████▆▅▄▅▅▆▆▆▄▃▃▂▂▂▂▁▂▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁ ▃
#   10.5 μs         Histogram: frequency by time        20.4 μs <

#  Memory estimate: 7.81 KiB, allocs estimate: 70.

# BenchmarkTools.Trial: 10000 samples with 5 evaluations.
#  Range (min … max):  6.476 μs … 947.349 μs  ┊ GC (min … max): 0.00% … 98.80%
#  Time  (median):     6.871 μs               ┊ GC (median):    0.00%
#  Time  (mean ± σ):   7.799 μs ±  16.118 μs  ┊ GC (mean ± σ):  3.56% ±  1.71%

#   ▅▅▁▄█▆▃ ▁▁          ▁▃▃▂▂▄▄▂▂▃▃▂▁▁▁ ▁▁▁▁▁  ▁                ▂
#   ███████████▇▅▇▅▅▆▇█▇███████████████████████████▇▆▇▅▅▅▅▄▄▅▄▄ █
#   6.48 μs      Histogram: log(frequency) by time      11.1 μs <

#  Memory estimate: 5.34 KiB, allocs estimate: 47.
